﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame {
    class GameFunctionalitiesImplementation : GameFunctionalities {

        public Game Initialize() {
            Console.WriteLine("Starting game...");
            Console.Write("Enter player 01 name: ");
            String player1Name=Console.ReadLine();
            Console.Write("Enter player 02 name: ");
            String player2Name=Console.ReadLine();
            Player player1 = new Player(player1Name, 0);
            Player player2 = new Player(player2Name, 0);
            Console.WriteLine("Players created.");

            Game game = new Game(player1, player2, player1, player2);
            List<Card> cardsList = GenerateCards();
            player1.cards = cardsList.GetRange(0, 10);
            cardsList.RemoveRange(0, 10);
            player2.cards = cardsList.GetRange(0, 10);
            cardsList.RemoveRange(0, 10);
            Console.WriteLine("Generated cards.");
            return game;
        }

        public void Terminate() {
            Console.WriteLine("Game ended. Thank you.");
        }

        public List<Card> GenerateCards() {
            List<Card> cardsList = new List<Card>();
            string chars = "ABCD";
            Random rand = new Random();

        start:
            while(cardsList.Count < 20) {
                int num = rand.Next(0, chars.Length - 1);
                char character = chars[num];
                byte numeral = (byte)rand.Next(1, 14);
                Card card = new Card(character, numeral);

                if(cardsList.Count > 0) {
                    for(int j = 0; j < cardsList.Count; j++) {
                        Card prevCard = cardsList.ElementAt(j);
                        if(prevCard.characterPart.Equals(card.characterPart) && prevCard.numeralPart.Equals(card.numeralPart)) {
                            goto start;
                        }
                    }
                    cardsList.Add(card);
                } else {
                    cardsList.Add(card);
                }
            }

            return cardsList;
        }

        public void Play(Game game) {
            Console.WriteLine(game.player1.name + " score: " + game.player1.score);
            Console.WriteLine(game.player2.name + " score: " + game.player2.score);
            Console.WriteLine("//------------------------------");

            while(game.player1.cards.Count > 0) {
                Player roundWinPlayer = game.roundWinner;
                Player roundNotWinPlayer = game.roundNotWinner;

                Card playedCard = roundWinPlayer.cards.ElementAt(0);
                roundWinPlayer.cards.Remove(playedCard);
                char playedCardChar = playedCard.characterPart;
                byte playedCardNum = playedCard.numeralPart;
                Console.WriteLine(roundWinPlayer.name + " " + playedCardChar + "" + playedCardNum);
                List<Card> notWinnerCards = roundNotWinPlayer.cards;
                List<Card> possibleCards = new List<Card>();
                for(int i = 0; i < notWinnerCards.Count; i++) {
                    if(notWinnerCards.ElementAt(i).characterPart.Equals(playedCard.characterPart) && notWinnerCards.ElementAt(i).numeralPart > playedCard.numeralPart) {
                        possibleCards.Add(notWinnerCards.ElementAt(i));
                    }
                }
                if(possibleCards.Count > 0) {
                    List<byte> possibleCardsNumerals = new List<byte>();
                    for(int i = 0; i < possibleCards.Count; i++) {
                        possibleCardsNumerals.Add(possibleCards.ElementAt(i).numeralPart);
                    }
                    byte minCardNum = possibleCardsNumerals.Min();
                    Console.WriteLine(roundNotWinPlayer.name + " " + playedCardChar + minCardNum);
                    for(int i = 0; i < notWinnerCards.Count; i++) {
                        Card adddedCard = notWinnerCards.ElementAt(i);
                        if(adddedCard.characterPart.Equals(playedCard.characterPart) && adddedCard.numeralPart == minCardNum) {
                            notWinnerCards.Remove(adddedCard);
                        }
                    }
                    game.roundWinner = roundNotWinPlayer;
                    game.roundNotWinner = roundWinPlayer;

                    game.roundWinner.score++;
                    Console.WriteLine("Winner : " + roundNotWinPlayer.name);
                    Console.WriteLine(game.player1.name + " score: " + game.player1.score);
                    Console.WriteLine(game.player2.name + " score: " + game.player2.score);
                } else {
                    List<byte> possibleCardsNumerals = new List<byte>();
                    for(int i = 0; i < notWinnerCards.Count; i++) {
                        possibleCardsNumerals.Add(notWinnerCards.ElementAt(i).numeralPart);
                    }
                    byte minCardNum = possibleCardsNumerals.Min();
                    Card card = null;
                    for(int i = 0; i < notWinnerCards.Count; i++) {
                        card = notWinnerCards.ElementAt(i);
                        if(card.numeralPart == minCardNum) {
                            goto continuePlay;
                        }
                    }

                continuePlay:
                    roundNotWinPlayer.cards.Remove(card);
                    Console.WriteLine(roundNotWinPlayer.name + " " + card.characterPart + card.numeralPart);
                    game.roundWinner.score++;
                    Console.WriteLine("Winner : " + game.roundWinner.name);
                    Console.WriteLine(game.player1.name + " score: " + game.player1.score);
                    Console.WriteLine(game.player2.name + " score: " + game.player2.score);
                }
                Console.WriteLine("//------------------------------");
            }
        }

        public Player DecideWinner(Game game) {
            Player player1 = game.player1;
            Player player2 = game.player2;
            if(player1.score > player2.score) {
                return player1;
            } else if(player2.score > player1.score) {
                return player2;
            } else {
                return null;
            }
        }
    }
}

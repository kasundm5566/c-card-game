﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame {
    interface GameFunctionalities {
        Game Initialize();
        void Terminate();
        List<Card> GenerateCards();
        void Play(Game game);
        Player DecideWinner(Game game);
    }
}

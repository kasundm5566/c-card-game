﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame {
    class Program {
        static void Main(string[] args) {
            GameFunctionalities gameFunctionalities = new GameFunctionalitiesImplementation();
            Game game = gameFunctionalities.Initialize();



            //List<Card> lst1=game.player1.cards;
            //List<Card> lst2 = game.player2.cards;

            //for(int i = 0; i < lst1.Count; i++) {
            //    Console.WriteLine("P1: "+lst1.ElementAt(i).characterPart + " " + lst1.ElementAt(i).numeralPart);
            //}
            //for(int i = 0; i < lst2.Count; i++) {
            //    Console.WriteLine("P2 :"+lst2.ElementAt(i).characterPart + " " + lst2.ElementAt(i).numeralPart);
            //}

            //gameFunctionalities.play(game);

            //lst1 = game.player1.cards;
            //lst2 = game.player2.cards;

            //for(int i = 0; i < lst1.Count; i++) {
            //    Console.WriteLine("P1: "+lst1.ElementAt(i).characterPart + " " + lst1.ElementAt(i).numeralPart);
            //}
            //for(int i = 0; i < lst2.Count; i++) {
            //    Console.WriteLine("P2: "+lst2.ElementAt(i).characterPart + " " + lst2.ElementAt(i).numeralPart);
            //}

            //List<Card> cards=gameFunctionalities.GenerateCards();
            //for (int i = 0; i < cards.Count; i++) {
            //    Console.WriteLine(cards.ElementAt(i).characterPart + " " + cards.ElementAt(i).numeralPart);
            //}

            gameFunctionalities.Play(game);

            Player player = gameFunctionalities.DecideWinner(game);
            if(player != null) {
                Console.WriteLine("Game winner: " + player.name);
            } else {
                Console.WriteLine("Game tied.");
            }

            gameFunctionalities.Terminate();
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame {
    class Player {
        public String name { get; set; }
        public byte score { get; set; }
        public List<Card> cards { get; set; }

        public Player(String name, byte score) {
            this.name = name;
            this.score = score;
        }     
    }
}

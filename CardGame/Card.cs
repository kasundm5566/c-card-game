﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame {
    class Card {
        public char characterPart { get; set; }
        public byte numeralPart { get; set; }

        public Card(char characterPart, byte numeralPart) {
            this.characterPart = characterPart;
            this.numeralPart = numeralPart;
        }
    }
}

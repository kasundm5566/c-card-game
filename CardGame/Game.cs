﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame {
    class Game {
        public Player player1 { get; set; }
        public Player player2 { get; set; }
        public Player roundWinner { get; set; }
        public Player roundNotWinner { get; set; }

        public Game(Player player1, Player player2,
            Player roundWinner,
            Player roundNotWinner) {
            this.player1 = player1;
            this.player2 = player2;
            this.roundWinner = roundWinner;
            this.roundNotWinner = roundNotWinner;
        }
    }
}
